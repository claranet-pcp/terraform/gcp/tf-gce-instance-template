# tf-gce-instance-template
Static IPs setup is available on the **static_ip** branch

## Variables
* `project` - Project Name in which template is going to be created
* `envname` -
* `service` -
* `domain` -
* `preemptible` (false) - Should instances created out of this template be
  preemptible or not (automatic_restart can't be true to use this option)
* `automatic_restart` (true) - Have to be **false** to allow **preemptible**
  option to be available (TF default is true)
* `startup_script` -
* `disk_image` -
* `net_name` -
* `network_enabled` (1) - is this template applicable for instances which are
  going to be placed in the single network (no subnets)
* `subnets_enabled` (0) - should this template be assigned to the instances
  which are going to be placed in subnetworks
* `fw_tags` -
* `needs_nat` (nat) -
* `machine_type` (g1-small) -
* `disk_device_name` (sda) -
* `scopes` (list of ["storage-ro", "compute-ro"]) -
* `ip_forward` (true) -
* `project` - GCP Project (used for image_family url generation)

## Outputs
* `link-net` - outputs instance template link created for network attached
  templates
* `link-subnets` - outputs instance template link created for subnetwork
  attached templates

## Usage examples
```
module "sso-instance-template" {
  source = "git::ssh://git@gogs.bashton.net/Bashton/tf-gce-instance-template.git"

  project           = "${var.gce_project}"
  network_enabled   = 0
  subnets_enabled   = 1
  preemptible       = "true"
  automatic_restart = "false"
  envname           = "${var.envname}"
  service           = "sso"
  domain            = "${var.envname}.${var.domain}"
  fw_tags           = ["sso"]
  needs_nat         = "natted"
  startup_script    = "not_sure_where_this_will_point"
  disk_image        = "${var.disk_image}"
  net_name          = "${var.envname}-${var.envtype}-nat-subnet"
  ip_forward        = "false"
}
```
